## Архитектура системы

![Architecture](img/MainSchema.svg "Architecture")



<img src="img/Diagram.svg" alt="Diagram" title="Diagram" style="zoom:150%;" />



## Процесс работы с системой

#### Local Operations

1. *(Пользователь)* Создание Issue с описанием проблемы/предложения
2. Рассмотрение и принятие Issue администратором
3. Создание Merge Request и ветки develop
4. Синхронизация локального и удаленного репозиториев
5. Редактирование файлов конфигурации и написание тестов
6. Проверка изменений в среде для разработки
7. *(В случае ошибки)* Исправление ошибок и повторная проверка

<img src="img/Git1.png" alt="Local Operations" title="Local Operations" style="zoom:150%;" />

#### Continuous Integration

1. Отправка директории проекта в удаленный репозиторий
2. Автоматический запуск Pipeline:
   - Валидация файлов
   - Сборка и отправка конфигураций (с заменой)
   - Тестирование
3. *(В случае ошибки)* Исправление ошибок и повторная отправка

<img src="img/Git2.png" alt="Continuous Integration" title="Continuous Integration" style="zoom:150%;" />

#### Continuous Delivery

1. Merge ветки develop с веткой master
2. Автоматический запуск Pipeline:
   - Сборка и отправка конфигураций (без замены)
3. Проверка вносимых изменений
4. Ручная отправка конфигураций (с заменой) и автоматизированные тесты
5. *(В случае ошибки)* Закрытие Merge Request и Revert

<img src="img/Git3.png" alt="Continuous Delivery" title="Continuous Delivery" style="zoom:150%;" />

### Общий вид схемы

<img src="img/Git4.png" alt="General View" title="General View" style="zoom:150%;" />
