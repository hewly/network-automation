dev-test: files-validation
	python3 testing/update_testbed.py --testbed testing/testbed.yml --cred group_vars/cisco.yml
	ansible-playbook apply_cfg.yml -e SEC_OCTET=16 -e COMMIT=false

dev-deploy:
	ansible-playbook apply_cfg.yml -e COMMIT=true -e SEC_OCTET=16
	robot --variable testbed:./testing/testbed.yml testing/validate_configuration.robot

files-validation:
	ansible-playbook apply_cfg.yml --syntax-check
	yamllint host_vars/*
	for device in `ls -1 host_vars/ER*`; do \
		pykwalify -d $$device -s testing/ios_routers_validation.yml; \
	done
	for device in `ls -1 host_vars/Spine*`; do \
		pykwalify -d $$device -s testing/ios_switches_validation.yml; \
	done
	for device in `ls -1 host_vars/Leaf*`; do \
		pykwalify -d $$device -s testing/ios_switchesl2_validation.yml; \
	done
	python3 ./testing/jinja2_validate.py templates/*/*/*

test-deploy:
	git add .
	git commit -m "${comment}"
	git push origin develop

sync:
	git checkout master
	git pull
	git branch -D develop
	git checkout -b develop
