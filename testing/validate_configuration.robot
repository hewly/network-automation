*** Settings ***
Library        ats.robot.pyATSRobot
Library        genie.libs.robot.GenieRobot
Library        unicon.robot.UniconRobot
Library		   String
Library        ping_test.py

*** Variables ***
${testbed}     testbed.yml

*** Test Cases ***
Initialize
    use genie testbed "${testbed}"
    connect to devices "Spine1"
    connect to devices "Spine2"
    connect to devices "ER1"
    connect to devices "ER2"

Ping_Setup
    run testcase "ping_test.common_setup"
Ping_T1
    run testcase "ping_test.Ping_from_ER1_to_ER2"
Ping_T2
    run testcase "ping_test.Ping_from_ER2_to_ER1"

#Verify VLAN300 IP on Spine1
#    verify ip of interface  Spine1  vlan 300  192.169.0.1
#Verify VLAN300 IP on Spine2
#    verify ip of interface  Spine2  vlan 300  192.169.0.2
#
#HSRP 250 status check on Spine1
#    verify hsrp group  Spine1  300  250  192.169.0.254  Standby
#HSRP 250 status check on Spine2
#    verify hsrp group  Spine2  300  250  192.169.0.254  Active
#
#OSPF network status on ER1
#    verify ospf network	ER1		192.169.0.0		inter area		10.1.200.9 10.1.200.10
#OSPF network status on ER2
#    verify ospf network	ER1		192.169.0.0		inter area		10.1.200.9 10.1.200.10

*** Keywords ***
verify ip of interface
    [Arguments]     ${device}   ${int}  ${ip}
    ${cmd}=         Execute "show ip interface brief ${int}" on device "${device}"
    should contain  ${cmd}  ${ip}

verify hsrp group
    [Arguments]     ${device}  ${vlan}  ${group}  ${vip}  ${mode}
    ${cmd}=         execute "show standby vlan${vlan}" on device "${device}"
    should contain  ${cmd}  Vlan${vlan} - Group ${group}
    should contain  ${cmd}  State is ${mode}
    should contain  ${cmd}  Virtual IP address is ${vip}

verify ospf network
	[Arguments]		${device}	${network}	${type}		${next_hop}
	${cmd}=			execute "show ip route ${network}" on device "${device}"
	@{next_hops}=	Split String	${next_hop}		${SPACE}
	should contain	${cmd}	type ${type}
	FOR  ${item}  IN  @{next_hops}
		should contain	${cmd}	${item}
	END

#Verify OSPF Neighbors on ER1
#    verify count "2" "ospf neighbors" on device "ER1"
#Verify OSPF Neighbors on ER2
#    verify count "2" "ospf neighbors" on device "ER2"
#Verify OSPF Neighbors on Spine1
#    verify count "3" "ospf neighbors" on device "Spine1"
#Verify OSPF Neighbors on Spine2
#    verify count "3" "ospf neighbors" on device "Spine2"

#OSPF status check on router R1
#    verify count "1" "ospf neighbors" on device "R1"
#    ${cmd}=         execute "show ip ospf neighbor 192.168.0.2" on device "R1"
#    should contain  ${cmd}  In the area 0 via interface GigabitEthernet0/1
#    should contain  ${cmd}  Neighbor priority is 1, State is FULL


#Ping_T3
#    run testcase "ping_test.Ping_from_Spine1_to_ER1"
#Ping_T4
#    run testcase "ping_test.Ping_from_Spine2_to_ER2"

#Verify interfaces on ER1
#    verify count "4" "interface up" on device "ER1"
#Verify interfaces on Spine2
#    verify count "12" "interface up" on device "Spine2"
#Verify interfaces on Leaf1
#    verify count "5" "interface up" on device "Leaf1"
#Verify interfaces on Leaf2
#    verify count "5" "interface up" on device "Leaf2"
#Verify interfaces on Spine1
#    verify count "13" "interface up" on device "Spine1"
#Verify interfaces on ER2
#    verify count "4" "interface up" on device "ER2"

