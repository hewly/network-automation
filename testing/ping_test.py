import logging
import re

from pyats import aetest

log = logging.getLogger(__name__)

class common_setup(aetest.CommonSetup):

    @aetest.subsection
    def check_topology(self, testbed):
        ER1 = testbed.devices["ER1"]
        ER2 = testbed.devices["ER2"]

        self.parent.parameters.update(
                ER1 = ER1,
                ER2 = ER2
                )

    @aetest.subsection
    def connect(self,steps,  ER1, ER2):
        with steps.start(f"Connecting to {ER1.name}"):
            ER1.connect()
        with steps.start(f"Connecting to {ER2.name}"):
            ER2.connect()

class Ping_from_ER1_to_ER2(aetest.Testcase):
    @aetest.test
    def ping(self, ER1):
        result = ER1.execute("ping 10.1.200.2 repeat 5")
        match = re.search("Success rate is 0 percent", result)
        if bool(match):
            self.failed()

class Ping_from_ER2_to_ER1(aetest.Testcase):
    @aetest.test
    def ping(self, ER2):
        result = ER2.execute("ping 10.1.200.1 repeat 5")
        match = re.search("Success rate is 0 percent", result)
        if bool(match):
            self.failed()

if __name__ == '__main__':
    import argparse
    from pyats.topology import loader

    parser = argparse.ArgumentParser()
    parser.add_argument('--testbed', dest = 'testbed',
                        type = loader.load)

    args, unknown = parser.parse_known_args()

    aetest.main(**vars(args))
